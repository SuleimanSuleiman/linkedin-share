export const ShareLinkedInController = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {


        const code = req.body.code;
        const redirect_uri = req.body.redirect_uri;
        console.log(`code`, code)

        const send_req_to_fetch_access_token = await axios.post("https://www.linkedin.com/oauth/v2/accessToken", {
            grant_type: "authorization_code",
            code: code,
            client_id: "",
            client_secret: "",
            redirect_uri: redirect_uri,
        }, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            }
        }
        );

        const access_token = send_req_to_fetch_access_token?.data?.access_token;



        const fetch_user_info = await axios.get("https://api.linkedin.com/v2/userinfo", {
            headers: {
                Authorization: "Bearer " + access_token,
            }
        });

        const sub = fetch_user_info?.data?.sub;

        const registerUpload = await axios.post("https://api.linkedin.com/v2/assets?action=registerUpload", {
            "registerUploadRequest": {
                "recipes": [
                    "urn:li:digitalmediaRecipe:feedshare-image"
                ],
                "owner": `urn:li:person:${sub}`,
                "serviceRelationships": [
                    {
                        "relationshipType": "OWNER",
                        "identifier": "urn:li:userGeneratedContent"
                    }
                ]
            }
        },
            {
                headers: {
                    Authorization: "Bearer " + access_token,
                }
            });

        if (registerUpload) {

            const uploadUrl = await registerUpload?.data?.value.uploadMechanism['com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest'].uploadUrl;

            const filePath = path.join(__dirname, "../../uploads/shareLinkedin/linkedin2.jpg");
            const fileStream: any = await fs.createReadStream(filePath);

            const formData: FormData = new FormData();
            formData.append('file', fileStream);

            const media_upload = await axios.put(uploadUrl, formData, {
                headers: {
                    Authorization: "Bearer " + access_token,
                    'Content-Type': 'multipart/form-data',
                }
            });

            console.log(`media_upload`, media_upload.data)

            const post_new = await axios.post("https://api.linkedin.com/v2/ugcPosts",
                {
                    "author": `urn:li:person:${sub}`,
                    "lifecycleState": "PUBLISHED",
                    "specificContent": {
                        "com.linkedin.ugc.ShareContent": {
                            "shareCommentary": {
                                "text": "Hello"
                            },
                            "shareMediaCategory": "IMAGE",
                            "media": [
                                {
                                    "status": "READY",
                                    "media": registerUpload.data?.value?.asset,
                                }
                            ]
                        }
                    },
                    "visibility": {
                        "com.linkedin.ugc.MemberNetworkVisibility": "PUBLIC"
                    }
                },
                {
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: "Bearer " + access_token,
                    }
                }
            )

            return res.status(200).send("successfully")

        } else {
            return next(HandleError(400, { "message": "Internal Error" }))
        }


    } catch (error: any) {
        console.log(error.message)
        return next(HandleError(400, { "message": "Internal Error" }))
    }
}
